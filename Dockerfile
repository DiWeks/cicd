FROM node:16 AS builder

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

RUN npx prisma generate

FROM node:16-alpine

COPY --from=builder /usr/src/app .

EXPOSE 3001

CMD ["npm", "start"]